package com.costyhundell.rotacloudapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.costyhundell.rotacloudapp.models.RocketResponse
import com.costyhundell.rotacloudapp.util.SpaceRocketsApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SpaceRocketsRepository(spaceRocketsApi: SpaceRocketsApi) {

    private val rocketResponseLiveData = MutableLiveData<RocketResponse>()

    private var rocketResponse =
        spaceRocketsApi.getRockets().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    private val disposable = rocketResponse.subscribe { response: RocketResponse?, error: Throwable? ->
        rocketResponseLiveData.postValue(response)
    }

    fun getRocketsResponse(): LiveData<RocketResponse> = rocketResponseLiveData

    fun cleanUp() {
        disposable?.dispose()
    }
}
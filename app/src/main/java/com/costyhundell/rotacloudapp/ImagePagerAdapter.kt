package com.costyhundell.rotacloudapp

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.costyhundell.rotacloudapp.util.inflate
import com.facebook.drawee.view.SimpleDraweeView

class ImagePagerAdapter(private val imageURLS: List<String>): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = container.inflate(R.layout.image_view_layout)
        val image = view.findViewById<SimpleDraweeView>(R.id.image_view)
        image.setImageURI(imageURLS[position])
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = `object` == view

    override fun getCount(): Int = imageURLS.size
}
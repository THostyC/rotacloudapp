package com.costyhundell.rotacloudapp

import android.view.View
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.costyhundell.rotacloudapp.models.Rocket
import kotlinx.android.synthetic.main.rocket_card.view.*

class RocketViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val rocketName = itemView.rocket_name
    val rocketHeightMetres = itemView.rocket_height_mt
    val rocketHeightFeet = itemView.rocket_height_ft
    val viewPager = itemView.image_pager
    val viewPagerIndicator = itemView.image_page_indicator
    val imageButton = itemView.show_pictures_button

    fun bind(item: Rocket, viewPagerAdapter: ImagePagerAdapter) {

        viewPager.adapter = viewPagerAdapter
        viewPagerIndicator.setViewPager(viewPager)

        rocketName.text = "Name: ${item.name}"
        rocketHeightFeet.text = "${item.height.feet} ft"
        rocketHeightMetres.text = "${item.height.meters} m"

        imageButton.setOnClickListener {
            when (viewPager.visibility) {
                View.VISIBLE -> {
                    viewPager.visibility = View.GONE
                    viewPagerIndicator.visibility = View.GONE
                }
                View.GONE -> {
                    viewPager.animation = AnimationUtils.loadAnimation(itemView.context, R.anim.slide_up)
                    viewPagerIndicator.animation = AnimationUtils.loadAnimation(itemView.context, R.anim.slide_up)
                    viewPager.visibility = View.VISIBLE
                    viewPagerIndicator.visibility = View.VISIBLE
                }
            }
        }
    }

}

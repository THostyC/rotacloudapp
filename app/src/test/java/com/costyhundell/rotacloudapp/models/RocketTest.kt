package com.costyhundell.rotacloudapp.models

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations.initMocks

class RocketTest {

    @Mock
    private val mockHeightObject = Mockito.mock(HeightObject::class.java)

    private lateinit var rocket: Rocket

    @Before
    fun before() {
        initMocks(this)
        rocket = Rocket(1, mockHeightObject, emptyList(), "")
    }

    @Test
    fun rocketIdTest() {
        assert(rocket.id == 1)
    }

    @Test
    fun rocketHeightObjectTest() {
        assert(rocket.height == mockHeightObject)
    }

    @Test
    fun rocketImageUrlsTest() {
        assert(rocket.images == emptyList<String>())
    }

    @Test
    fun rocketNameTest() {
        assert(rocket.name == "")
    }



}
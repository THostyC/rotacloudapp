package com.costyhundell.rotacloudapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() , LifecycleOwner {
    override fun getLifecycle(): Lifecycle = LifecycleRegistry(this)

    private val spaceRocketsViewModel: SpaceRocketsViewModel by viewModel()
    private var adapter: SpaceRocketsAdapter? =  null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rocket_rv.apply {
            layoutManager = LinearLayoutManager(context)
        }

        reverse_order.apply {
            setOnClickListener {
                spaceRocketsViewModel.reverseList()
            }
        }
        spaceRocketsViewModel
            .getRocketsList()
            .observeForever { rocketList ->
                if (adapter == null) {
                    adapter = SpaceRocketsAdapter(rocketList)
                    rocket_rv.adapter = adapter
                } else {
                    adapter!!.submitList(rocketList)
                }
            }
    }

    override fun onStop() {
        super.onStop()
        spaceRocketsViewModel.clear()
    }

}

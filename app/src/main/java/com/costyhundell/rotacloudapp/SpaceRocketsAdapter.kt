package com.costyhundell.rotacloudapp

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import com.costyhundell.rotacloudapp.models.Rocket
import com.costyhundell.rotacloudapp.util.inflate

class SpaceRocketsAdapter(rocketList: List<Rocket>?) : ListAdapter<Rocket, RocketViewHolder>(DIFF_UTIL) {

    init {
        submitList(rocketList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RocketViewHolder =
        RocketViewHolder(parent.inflate(R.layout.rocket_card))

    override fun onBindViewHolder(holder: RocketViewHolder, position: Int) {
        val item = getItem(position)
        val imagePageAdapter = ImagePagerAdapter(item.images)
        holder.bind(item, imagePageAdapter)
    }

    companion object {
        val DIFF_UTIL = object : ItemCallback<Rocket>() {
            override fun areItemsTheSame(oldItem: Rocket, newItem: Rocket): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Rocket, newItem: Rocket): Boolean {
                return oldItem === newItem
            }

        }
    }
}

package com.costyhundell.rotacloudapp.models

import org.junit.Test

class RocketResponseTest {
    private val rocketResponse = RocketResponse(emptyList())

    @Test
    fun testRocketResponse() {
        assert(rocketResponse.data == emptyList<Rocket>())
    }
}
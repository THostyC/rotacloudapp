package com.costyhundell.rotacloudapp

import android.app.Application
import com.costyhundell.rotacloudapp.di.networkModule
import com.costyhundell.rotacloudapp.di.repositoryModule
import com.costyhundell.rotacloudapp.di.viewModelModule
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.android.startKoin

open class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this@BaseApplication, listOf(viewModelModule, repositoryModule, networkModule))
        Fresco.initialize(this)
    }
}

package com.costyhundell.rotacloudapp.util

import com.costyhundell.rotacloudapp.models.Rocket
import com.costyhundell.rotacloudapp.models.RocketResponse
import com.google.gson.*
import java.lang.reflect.Type

class DataAdapter: JsonDeserializer<RocketResponse> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): RocketResponse {
        val jsonArray = json?.asJsonArray
        val result = mutableListOf<Rocket>()
        jsonArray?.forEach {
            result.add(Gson().fromJson(it, Rocket::class.java))
        }
        return RocketResponse(result)
    }
}
package com.costyhundell.rotacloudapp.view_holders

import android.content.Context
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.costyhundell.rotacloudapp.ImagePagerAdapter
import com.costyhundell.rotacloudapp.RocketViewHolder
import com.costyhundell.rotacloudapp.models.HeightObject
import com.costyhundell.rotacloudapp.models.Rocket
import com.viewpagerindicator.LinePageIndicator
import kotlin.test.assertTrue
import kotlinx.android.synthetic.main.rocket_card.view.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class SpaceRocketsViewHolderTest {
    val mockItemView = mock(View::class.java)
    val mockContext = mock(Context::class.java)
    val mockRocketName = mock(TextView::class.java)
    val mockHeightMetres = mock(TextView::class.java)
    val mockHeightFeet = mock(TextView::class.java)
    val mockViewPager = mock(ViewPager::class.java)
    val mockIndicator = mock(LinePageIndicator::class.java)
    val mockImageButton = mock(ImageButton::class.java)

    val viewPagerAdapter = ImagePagerAdapter(emptyList())

    val rocket = Rocket(1, HeightObject(0f,0f), emptyList(), "Rocket")

    var rocketViewHolder: RocketViewHolder? = null

    @Before
    fun setUp() {
        `when`(mockItemView.rocket_name).thenReturn(mockRocketName)
        `when`(mockItemView.rocket_height_ft).thenReturn(mockHeightFeet)
        `when`(mockItemView.rocket_height_mt).thenReturn(mockHeightMetres)
        `when`(mockItemView.image_pager).thenReturn(mockViewPager)
        `when`(mockItemView.image_page_indicator).thenReturn(mockIndicator)
        `when`(mockItemView.show_pictures_button).thenReturn(mockImageButton)
        `when`(mockItemView.context).thenReturn(mockContext)
        `when`(mockViewPager.adapter).thenReturn(viewPagerAdapter)
        `when`(mockRocketName.text).thenReturn("Name: ${rocket.name}")
        `when`(mockHeightFeet.text).thenReturn("${rocket.height.feet} ft")
        `when`(mockHeightMetres.text).thenReturn("${rocket.height.meters} m")

        rocketViewHolder = RocketViewHolder(mockItemView)
        rocketViewHolder!!.bind(rocket, viewPagerAdapter)
    }

    @Test
    fun testBindCalls() {
        verify(mockViewPager).adapter = viewPagerAdapter
        verify(mockIndicator).setViewPager(mockViewPager)
        verify(mockRocketName).text = "Name: ${rocket.name}"
        verify(mockHeightFeet).text = "${rocket.height.feet} ft"
        verify(mockHeightMetres).text = "${rocket.height.meters} m"

        assertTrue {
            mockViewPager.adapter == viewPagerAdapter
        }

        assertTrue {
            mockRocketName.text == "Name: ${rocket.name}"
        }

        assertTrue {
            mockHeightFeet.text == "${rocket.height.feet} ft"
        }

        assertTrue {
            mockHeightMetres.text == "${rocket.height.meters} m"
        }
    }
}
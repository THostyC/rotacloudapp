package com.costyhundell.rotacloudapp.di


import com.costyhundell.rotacloudapp.*
import com.costyhundell.rotacloudapp.models.RocketResponse
import com.costyhundell.rotacloudapp.util.DataAdapter
import com.costyhundell.rotacloudapp.util.SpaceRocketsApi
import com.google.gson.GsonBuilder
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private val retrofit: Retrofit = createClient()

private val spaceRocketsApi: SpaceRocketsApi = retrofit.create(
    SpaceRocketsApi::class.java)

val viewModelModule = module {
    viewModel { SpaceRocketsViewModel(get()) }
}

val repositoryModule = module {
    single { SpaceRocketsRepository(spaceRocketsApi = get()) }
}

val networkModule = module {
    single { spaceRocketsApi }
}

private fun createClient() = Retrofit.Builder()
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().registerTypeAdapter(
        RocketResponse::class.java,
        DataAdapter()
    ).create()))
    .baseUrl("https://api.spacexdata.com/v3/")
    .build()

package com.costyhundell.rotacloudapp

import androidx.lifecycle.*
import com.costyhundell.rotacloudapp.models.Rocket
import com.costyhundell.rotacloudapp.models.RocketResponse

class SpaceRocketsViewModel(val spaceRocketsRepository: SpaceRocketsRepository) : ViewModel(), LifecycleOwner {
    override fun getLifecycle(): LifecycleRegistry = LifecycleRegistry(this)

    private val rocketResponse: LiveData<RocketResponse> = spaceRocketsRepository.getRocketsResponse()
    private val rockets = MutableLiveData<List<Rocket>>().apply {
        rocketResponse.observeForever { response ->
            this.postValue(response.data.sortedByDescending {
                it.height.feet
            })
        }
    }

    fun clear() {
        spaceRocketsRepository.cleanUp()
    }

    fun getRocketsList(): LiveData<List<Rocket>> = rockets

    fun reverseList() {
        rockets.apply {
            postValue(rockets.value?.reversed())
        }
    }
}
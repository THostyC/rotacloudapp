package com.costyhundell.rotacloudapp.models

import org.junit.Test

class HeightObjectTest {

    private val metres = 0.5f
    private val feet = 1.5f
    private val heightObject = HeightObject(metres,feet)

    @Test
    fun testMetres() {
        assert(heightObject.meters == metres)
    }

    @Test
    fun testFeet() {
        assert(heightObject.feet == feet)
    }

}
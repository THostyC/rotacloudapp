package com.costyhundell.rotacloudapp.util

import com.costyhundell.rotacloudapp.models.RocketResponse
import io.reactivex.Single
import retrofit2.http.GET

interface SpaceRocketsApi {
    @GET("rockets")
    fun getRockets(): Single<RocketResponse>
}
